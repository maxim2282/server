const express = require('express')
const mongoose = require('mongoose')


const PORT = process.env.PORT || 5000
const DB_URL = 'mongodb://admin:admin@localhost:27017'


const app = express()

app.get('/', (req, res) => {
    res.status(200).send("Hey")
})



const start = async () => {
    try{
        await mongoose.connect(DB_URL)
        app.listen(PORT, () => console.log(`server started on port ${PORT}`))
    } catch (e) {
        console.log(e)
    }
}

start()